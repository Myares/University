# Celebrating 2016 in GitLab

---

# Licenses

---

#[fit] Open Source is not equal to Free(dom)

---

#[fit] MIT Expat License

## **Do whatever you want**

#### But include the license and copyright

---

```
Copyright (c) 2011-2016 GitLab B.V.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
```

---

#[fit] GitLab EE is proprietary

---

```
The GitLab Enterprise Edition (EE) license (the “EE License”)
Copyright (c) 2011-2016 GitLab B.V.

This software and associated documentation files (the "Software") may only be
used if you (and any entity that you represent) have agreed to, and are in
compliance with, the GitLab Subscription Terms of Service, available at
https://about.gitlab.com/terms/#subscription (the “EE Terms”), and otherwise
have a valid GitLab Enterprise Edition subscription for the correct number of
user seats. Subject to the foregoing sentence, you are free to modify this
Software and publish patches to the Software. You agree that GitLab and/or its
licensors (as applicable) retain all right, title and interest in and to all
Software incorporated in such modifications and/or patches, and all such
Software may only be used, copied, modified, displayed, distributed, or
otherwise exploited with a valid GitLab Enterprise Edition subscription for the
correct number of user seats.  Subject to the foregoing, it is forbidden to
copy, merge, publish, distribute, sublicense, and/or sell the Software.

This EE License applies only to the part of this Software that is not
distributed as part of GitLab Community Edition (CE), and that is not a file
that produces client-side JavaScript, in whole or in part. Any part of this
Software distributed as part of GitLab CE or that is a file that produces
client-side JavaScript, in whole or in part, is copyrighted under the MIT Expat
license. The full text of this EE License shall be included in all copies or
substantial portions of the Software.

```
---

# Other licenses

---

# Apache

The Apache License is a permissive license similar to the MIT License, but also provides an express grant of patent rights from contributors to users.

E.g. Apache, SVN

---

# GPL

The GPL (V2 or V3) is a copyleft license that requires anyone who distributes your code or a derivative work to make the source available under the same terms. V3 is similar to V2, but further restricts use in hardware that forbids software alterations.

E.g. Linux, Git, Wordpress

---

#[fit] __choose__a__license__.com
